//
// Created by viking on 5/3/19.
//

#ifndef BACHELOR_IMAGE_H
#define BACHELOR_IMAGE_H

#include <string>
#include <opencv2/opencv.hpp>
#include "ParsePTS.h"


class Image {

public:
    static ParsePTS p;
    cv::Mat glViewMatrix;

    Image(std::string);

    void draw(std::string);
    void save(std::string);

private:
    std::string fileName;
    cv::Mat im;
    double focalLength;
    cv::Point2d center;
    cv::Mat cameraMatrix;
    cv::Mat distCoeffs;

    // Image and Model Vertices
    std::vector<cv::Point2d> imagePoints;
    std::vector<cv::Point3d> modelPoints;

    // Texture faces
    std::vector<cv::Point2d> sourcePoints;
    std::vector<cv::Point2d> destinationPoints;
    std::vector<cv::Mat> faceTextures;

    // Object transformation
    cv::Mat rotationVector;
    cv::Mat translationVector;

    // Line Points for drawing coordinate system
    std::vector<cv::Point3d> line_end_point3D;
    std::vector<cv::Point2d> line_end_point2D;

    void create();
    void calculateTransformation(int);
    void getFaceTextures();
    void getFaceTexturesCombined();
    void drawCoordinateSystem();
    void drawBoundingBox();

};


#endif //BACHELOR_IMAGE_H
