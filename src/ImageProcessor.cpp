//
// Created by viking on 5/25/19.
//

#include "ImageProcessor.h"


ImageProcessor::ImageProcessor(std::string fileName) {
    im = cv::imread(fileName, cv::IMREAD_GRAYSCALE);
    this->fileName = fileName;
}


void ImageProcessor::lsd_lines(bool useRefine, bool useCanny, bool overlay) {

    cv::imshow("source", im);

    if(useCanny) {
        cv::Canny(im, im, 50, 200, 3);
    }

    // Create LSD detector with standard or no refinement
    cv::Ptr<cv::LineSegmentDetector> ls = cv::createLineSegmentDetector((useRefine ? cv::LSD_REFINE_STD : cv::LSD_REFINE_NONE));

    double start = double(cv::getTickCount());
    std::vector<cv::Vec4f> lines_std;

    // Detect the lines
    ls->detect(im, lines_std);

    double duration_ms = ((double(cv::getTickCount()) - start) * 1000 / cv::getTickFrequency());
    std::cout << "it took" << duration_ms << "ms." << std::endl;

    // Show found lines
    if(!overlay || useCanny) {
        im = cv::Scalar(0, 0, 0);
    }

    ls->drawSegments(im, lines_std);
    cv::String window_name = useRefine ? "Result - standard refinement" : "Result - no refinement";
    window_name += useCanny ? " - Canny edge detector used" : "";

    cv::imshow(window_name, im);
}

void ImageProcessor::hough_lines(bool probabilistic) {
    cv::Mat dst, cdst;
    cv::Canny(im, dst, 50, 200, 3);
    cv::cvtColor(dst, cdst, cv::COLOR_GRAY2BGR);

    if(!probabilistic) {
        std::vector<cv::Vec2f> lines;
        cv::HoughLines(dst, lines, 1, CV_PI/180, 90, 0, 0);

        for(int i = 0; i < lines.size(); i++) {
            float rho = lines[i][0], theta = lines[i][1];
            cv::Point pt1, pt2;
            double a = cos(theta), b = sin(theta);
            double x0 = a*rho, y0 = b*rho;

            pt1.x = cvRound(x0 + 1000 * (-b));
            pt1.y = cvRound(y0 + 1000 * (a));
            pt2.x = cvRound(x0 - 1000 * (-b));
            pt2.y = cvRound(y0 - 1000 * (a));

            cv::line(cdst, pt1, pt2, cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
        }
    }
    else {
        std::vector<cv::Vec4i> lines;
        cv::HoughLinesP(dst, lines, 1, CV_PI/180, 50, 50, 10);
        for(int i = 0; i < lines.size(); i++) {
            cv::Vec4i l = lines[i];
            cv::line(cdst, cv::Point(l[0], l[1]), cv::Point(l[2], l[3]), cv::Scalar(0, 0, 255), 3, cv::LINE_AA);
        }
    }

    cv::imshow("src", im);
    cv::imshow("detected lines", cdst);
}

// edge specific variables
int edgeThresh = 1;
int edgeThreshScharr = 1;

cv::Mat image, gray, blurImage, edge1, edge2, cedge;

const char* window_name1 = "Edge map : Canny default (Sobel gradient)";
const char* window_name2 = "Edge map : Canny with custom gradient (Scharr)";

static void onTrackbar(int, void *) {
    cv::blur(gray, blurImage, cv::Size(3, 3));

    // Run the edge detector on grayscale
    cv::Canny(blurImage, edge1, edgeThresh, edgeThresh*3, 3);
    cedge = cv::Scalar::all(0);

    image.copyTo(cedge, edge1);
    cv::imshow(window_name1, cedge);

    cv::Mat dx, dy;
    cv::Scharr(blurImage, dx, CV_16S, 1, 0);
    cv::Scharr(blurImage, dy, CV_16S, 0, 1);
    cv::Canny(dx, dy, edge2, edgeThreshScharr, edgeThreshScharr*3);
    cedge = cv::Scalar::all(0);
    image.copyTo(cedge, edge2);
    cv::imshow(window_name2, cedge);
}

void ImageProcessor::edge() {

    image = cv::imread(fileName, cv::IMREAD_COLOR);

    cedge.create(image.size(), image.type());
    cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

    cv::namedWindow(window_name1, 1);
    cv::namedWindow(window_name2, 1);

    cv::createTrackbar("Canny threshold default", window_name1, &edgeThresh, 100, onTrackbar);
    cv::createTrackbar("Canny threshold Scharr", window_name2, &edgeThreshScharr, 400, onTrackbar);

    onTrackbar(0, 0);

}