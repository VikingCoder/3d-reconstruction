//
// Created by viking on 5/3/19.
//

#include "Image.h"


ParsePTS Image::p;


Image::Image(std::string fileName) {
    this->fileName = fileName;

    create();
}

void Image::draw(std::string title) {
    cv::imshow(title, im);
}

void Image::save(std::string fileName) {
    cv::imwrite(fileName, im);
}

void Image::create() {
    // Parse pts file
    p.parse("assets/cube/" + fileName + ".pts");

    imagePoints = p.getImagePoints();
    modelPoints = p.getModelPoints();
    sourcePoints = p.getSourcePoints();
    destinationPoints = p.getDestinationPoints();
    focalLength = p.getFocalLength();

    // Image stuff
    im = cv::imread("assets/cube/" + fileName + ".JPG");
    center = cv::Point2d(im.cols / 2.0, im.rows / 2.0);
    cameraMatrix = (cv::Mat_<double>(3, 3) << focalLength, 0, center.x, 0, focalLength, center.y, 0, 0, 1);
    distCoeffs = cv::Mat::zeros(4, 1, cv::DataType<double>::type);

    calculateTransformation(6);
    getFaceTextures();
    getFaceTexturesCombined();
    drawCoordinateSystem();
    //drawBoundingBox();
}

void Image::calculateTransformation(int nPoints) {
    // TODO: Add different algorithms for the solutions
    // TODO: Use Information to render camera in 3D
    // Output rotation and translation
    // if only three points are used
    if(nPoints == 3) {
        cv::Matx13d rotation_vector;
        cv::Matx13d translation_vector;

        cv::solvePnP(modelPoints, imagePoints, cameraMatrix, distCoeffs, rotation_vector, translation_vector, true);

        rotationVector = cv::Mat(rotation_vector);
        translationVector = cv::Mat(translation_vector);
    }
    else if(nPoints > 3) {
        cv::Mat rotation_vector;
        cv::Mat translation_vector;

        auto start = std::chrono::high_resolution_clock::now();
        cv::solvePnP(modelPoints, imagePoints, cameraMatrix, distCoeffs, rotation_vector, translation_vector, false, cv::SOLVEPNP_EPNP);
        auto end = std::chrono::high_resolution_clock::now();

        std::cout << "f() took "
                  << std::chrono::duration_cast<std::chrono::microseconds>(end-start).count()
                  << " microseconds\n";

        rotationVector = rotation_vector;
        translationVector = translation_vector;
    }
    else return;

//    cv::Mat mt;
//
//    cv::Rodrigues(rotationVector, mt);
//
//    std::cout << rotationVector << std::endl;
//    std::cout << translationVector << std::endl;
//    std::cout << mt << std::endl;
    //std::cout << rotation_vector.type() << std::endl;
    //std::cout << translation_vector.type() << std::endl;

    // Create view matrix for OpenGL
    cv::Mat rotation, viewMatrix;
    cv::Rodrigues(rotationVector, rotation);

    viewMatrix = cv::Mat::zeros(4, 4, CV_64F);

    std::cout << rotationVector << std::endl;
    std::cout << translationVector << std::endl;

    // buil view matrix from rotation and translation vectors
    for(int row = 0; row < 3; row++) {
        for(int col = 0; col < 3; col++) {
            viewMatrix.at<double>(row, col) = rotation.at<double>(row, col);
        }
        viewMatrix.at<double>(row, 3) = translationVector.at<double>(row, 0);
    }

    viewMatrix.at<double>(3, 3) = 1.0f;

    // Multiply view matrix with the transfer matrix between OpenCV and OpenGL

    cv::Mat cvtoGl = cv::Mat::zeros(4, 4, CV_64F);
    cvtoGl.at<double>(0, 0) = 1.0f;
    cvtoGl.at<double>(1, 1) = -1.0f; // Invert the y axis
    cvtoGl.at<double>(2, 2) = -1.0f; // Invert the z axis
    cvtoGl.at<double>(3, 3) = 1.0f;

    viewMatrix = cvtoGl * viewMatrix;

    glViewMatrix = cv::Mat::zeros(4, 4, CV_64F);
    cv::transpose(viewMatrix, glViewMatrix);
    //glViewMatrix = viewMatrix;
}

void Image::getFaceTextures() {
    int numFaces = sourcePoints.size() / 4;

    cv::Point2f src[4];
    cv::Point2f dst[4];

    dst[0] = destinationPoints[0];
    dst[1] = destinationPoints[1];
    dst[2] = destinationPoints[2];
    dst[3] = destinationPoints[3];

    cv::Mat dest;

    for(int i = 0; i < numFaces; i++) {
        src[0] = sourcePoints[0 + 4 * i];
        src[1] = sourcePoints[1 + 4 * i];
        src[2] = sourcePoints[2 + 4 * i];
        src[3] = sourcePoints[3 + 4 * i];

        cv::Mat m = cv::getPerspectiveTransform(src, dst);

        std::cout << m << std::endl;

        cv::warpPerspective(im, dest, m, p.getSize());

        cv::imwrite("tex" + std::to_string(i) + ".jpg", dest);
        faceTextures.emplace_back(dest.clone());
    }
}

void Image::getFaceTexturesCombined() {

    cv::Mat subTexture1;
    cv::Mat subTexture2;
    cv::Mat fullTexture;
    cv::Mat empty(cv::Size(300, 300), faceTextures[2].type());
    empty = cv::Scalar(0, 0, 0);

    std::vector<cv::Mat> tex;

    tex.emplace_back(faceTextures[0]);
    tex.emplace_back(faceTextures[1]);

    cv::hconcat(tex, subTexture1);

    tex.clear();

    tex.emplace_back(empty);
    tex.emplace_back(faceTextures[2]);

    cv::hconcat(tex, subTexture2);

    cv::vconcat(subTexture2, subTexture1, fullTexture);

    cv::imwrite("combinedTexture.jpg", fullTexture);
}

void Image::drawCoordinateSystem() {
    line_end_point3D.emplace_back(cv::Point3d(1.0, 0, 0));
    line_end_point3D.emplace_back(cv::Point3d(0, 1.0, 0));
    line_end_point3D.emplace_back(cv::Point3d(0, 0, 1.0));

    line_end_point3D.emplace_back(cv::Point3d(1.0, 1.0, 0));
    line_end_point3D.emplace_back(cv::Point3d(0, 1.0, 1.0));
    line_end_point3D.emplace_back(cv::Point3d(1.0, 1.0, 1.0));
    line_end_point3D.emplace_back(cv::Point3d(1.0, 0, 1.0));

    cv::projectPoints(line_end_point3D, rotationVector, translationVector, cameraMatrix, distCoeffs, line_end_point2D);

    for(int i = 0; i < imagePoints.size(); i++) {
        cv::circle(im, imagePoints[i], 3, cv::Scalar(0, 0, 255), -1);
    }

    // Front square
    cv::line(im, imagePoints[0], line_end_point2D[0], cv::Scalar(255, 0, 0), 2);
    cv::line(im, imagePoints[0], line_end_point2D[1], cv::Scalar(0, 255, 0), 2);
    cv::line(im, imagePoints[0], line_end_point2D[2], cv::Scalar(0, 0, 255), 2);
}

void Image::drawBoundingBox() {
    cv::line(im, line_end_point2D[1], line_end_point2D[3], cv::Scalar(255, 0, 0), 2);
    cv::line(im, line_end_point2D[0], line_end_point2D[3], cv::Scalar(255, 0, 0), 2);

    //Back square
    cv::line(im, line_end_point2D[2], line_end_point2D[6], cv::Scalar(255, 0, 0), 1);
    cv::line(im, line_end_point2D[6], line_end_point2D[5], cv::Scalar(255, 0, 0), 1);
    cv::line(im, line_end_point2D[5], line_end_point2D[4], cv::Scalar(255, 0, 0), 1);
    cv::line(im, line_end_point2D[4], line_end_point2D[2], cv::Scalar(255, 0, 0), 1);

    //Connecting edges
    cv::line(im, line_end_point2D[0], line_end_point2D[6], cv::Scalar(255, 0, 0), 1);
    cv::line(im, line_end_point2D[3], line_end_point2D[5], cv::Scalar(255, 0, 0), 1);
    cv::line(im, line_end_point2D[1], line_end_point2D[4], cv::Scalar(255, 0, 0), 1);
}