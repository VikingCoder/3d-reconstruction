//
// Created by viking on 5/25/19.
//

#ifndef BACHELOR_IMAGEPROCESSOR_H
#define BACHELOR_IMAGEPROCESSOR_H


#include <opencv2/opencv.hpp>

class ImageProcessor {
public:

    cv::Mat im;
    std::string fileName;

    ImageProcessor(std::string fileName);

    void lsd_lines(bool, bool, bool);
    void hough_lines(bool);
    void edge();

private:

};


#endif //BACHELOR_IMAGEPROCESSOR_H
