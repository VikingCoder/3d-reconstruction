#include <opencv2/opencv.hpp>
#include "ParsePTS.h"
#include "Image.h"
#include "rendering/ObjectRenderer.h"
#include "ImageProcessor.h"
#include <thread>

//TODO: Find out how to use this with only 3 points
//TODO: Understand the backend of OpenCV
//TODO: Transform the object in OpenGL like in the picture and check how it looks
//TODO: Find out how to reconstruct other primitive objects
//TODO: Render Camera Frustum

int main() {

    Image i("RIMG0017");
    ObjectRenderer o;
    //ImageProcessor l("assets/cube/RIMG0017.JPG");
    //l.edge();
    //l.lsd_lines(true, false, false);
    //l.hough_lines(false);


    i.draw("Test");
    i.save("test.jpg");

    o.init(i.glViewMatrix);
    o.run();

    cv::waitKey(0);


    return 1;

}