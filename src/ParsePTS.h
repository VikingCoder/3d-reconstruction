//
// Created by viking on 4/24/19.
//

#ifndef BACHELOR_PARSEPTS_H
#define BACHELOR_PARSEPTS_H


#include <string>
#include <opencv2/core/types.hpp>

class ParsePTS {

public:
    void parse(const std::string &fileName);

    std::vector<cv::Point2d> getImagePoints();
    std::vector<cv::Point3d> getModelPoints();
    std::vector<int> getFaces();
    int getVerticesPerFace();

    std::vector<cv::Point2d> getSourcePoints();
    std::vector<cv::Point2d> getDestinationPoints();

    float getWidthX();
    float getHeightY();
    float getDepthZ();

    cv::Size getSize();

    double getFocalLength();

private:
    // Focal-Length
    double focalLength;

    // Size
    float sizeX;
    float sizeY;

    // Objectsize
    float widthX;
    float heightY;
    float depthZ;

    // Image and Model Vertices
    std::vector<cv::Point2d> imagePoints;
    std::vector<cv::Point3d> modelPoints;
    std::vector<int> faces;
    int verticesPerFace = 4;

    // Texture faces
    std::vector<cv::Point2d> sourcePoints;
    std::vector<cv::Point2d> destinationPoints;

    void addVertex(std::string);
    void addDestinationPoints(std::string);
    void addFace(std::string);
    void addFocalLength(std::string);
    void setCoords(std::string);

};


#endif //BACHELOR_PARSEPTS_H
