//
// Created by viking on 5/6/19.
//


#include "Camera.h"


Camera::Camera(glm::vec3 position, glm::vec3 up, float yaw, float pitch) :
        front(glm::vec3(0.0f, 0.0f, -1.0f)),
        movementSpeed(SPEED),
        mouseSensitivity(SENSITIVITY),
        zoom(ZOOM)
{
    this->position = position;
    this->worldUp = up;
    this->yaw = yaw;
    this->pitch = pitch;
    updateCameraVectors();
}

Camera::Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) :
        front(glm::vec3(0.0f, 0.0f, -1.0f)),
        movementSpeed(SPEED),
        mouseSensitivity(SENSITIVITY),
        zoom(ZOOM)
{
    this->position = glm::vec3(posX, posY, posZ);
    this->worldUp = glm::vec3(upX, upY, upZ);
    this->yaw = yaw;
    this->pitch = pitch;
    updateCameraVectors();
}

glm::mat4 Camera::GetViewMatrix() {

    glm::mat4 m = glm::lookAt(position, position + front, up);
//    std::cout << m[0][0] << m[0][1] << m[0][2] << m [0][3] << std::endl;
//    std::cout << m[1][0] << m[1][1] << m[1][2] << m [1][3] << std::endl;
//    std::cout << m[2][0] << m[2][1] << m[2][2] << m [2][3] << std::endl;
//    std::cout << m[3][0] << m[3][1] << m[3][2] << m [3][3] << std::endl;

    return glm::lookAt(position, position + front, up);
}

glm::mat4 Camera::GetViewMatrixPnP(cv::Mat glViewMatrix) {
    glm::mat4 m;

    m[0][0] = glViewMatrix.at<double>(0, 0);
    m[0][1] = glViewMatrix.at<double>(0, 1);
    m[0][2] = glViewMatrix.at<double>(0, 2);
    m[0][3] = glViewMatrix.at<double>(0, 3);

    m[1][0] = glViewMatrix.at<double>(1, 0);
    m[1][1] = glViewMatrix.at<double>(1, 1);
    m[1][2] = glViewMatrix.at<double>(1, 2);
    m[1][3] = glViewMatrix.at<double>(1, 3);

    m[2][0] = glViewMatrix.at<double>(2, 0);
    m[2][1] = glViewMatrix.at<double>(2, 1);
    m[2][2] = glViewMatrix.at<double>(2, 2);
    m[2][3] = glViewMatrix.at<double>(2, 3);

    m[3][0] = glViewMatrix.at<double>(3, 0);
    m[3][1] = glViewMatrix.at<double>(3, 1);
    m[3][2] = glViewMatrix.at<double>(3, 2);
    m[3][3] = glViewMatrix.at<double>(3, 3);

    //std::cout << glViewMatrix << std::endl;

//    std::cout << m[0][0] << " " << m[0][1] << " " << m[0][2] << " " << m [0][3] << std::endl;
//    std::cout << m[1][0] << " " << m[1][1] << " " << m[1][2] << " " << m [1][3] << std::endl;
//    std::cout << m[2][0] << " " << m[2][1] << " " << m[2][2] << " " << m [2][3] << std::endl;
//    std::cout << m[3][0] << " " << m[3][1] << " " << m[3][2] << " " << m [3][3] << std::endl;

    return m;
}

// Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
void Camera::ProcessKeyboard(Camera_Movement direction, float deltaTime) {
    float velocity = movementSpeed * deltaTime;
    if (direction == FORWARD)
        position += front * velocity;
    if (direction == BACKWARD)
        position -= front * velocity;
    if (direction == LEFT)
        position -= right * velocity;
    if (direction == RIGHT)
        position += right * velocity;
}

// Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
void Camera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch) {
    xoffset *= mouseSensitivity;
    yoffset *= mouseSensitivity;

    yaw   += xoffset;
    pitch += yoffset;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch)
    {
        if (pitch > 89.0f)
            pitch = 89.0f;
        if (pitch < -89.0f)
            pitch = -89.0f;
    }

    // Update Front, Right and up Vectors using the updated Euler angles
    updateCameraVectors();
}

// Processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
void Camera::ProcessMouseScroll(float yoffset) {
    if (zoom >= 1.0f && zoom <= 45.0f)
        zoom -= yoffset;
    if (zoom <= 1.0f)
        zoom = 1.0f;
    if (zoom >= 45.0f)
        zoom = 45.0f;
}

void Camera::updateCameraVectors() {
    glm::vec3 locFront;
    locFront.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    locFront.y = sin(glm::radians(pitch));
    locFront.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(locFront);
    // Also re-calculate the Right and Up vector
    right = glm::normalize(glm::cross(front, worldUp));  // Normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
    up    = glm::normalize(glm::cross(right, front));
}