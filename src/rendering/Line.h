//
// Created by viking on 7/9/19.
//

#ifndef BACHELOR_LINE_H
#define BACHELOR_LINE_H


#include <glad/glad.h>


class Line {
public:
    void init();
    void draw();

private:
    unsigned int vao = 0;
    unsigned int vbo = 0;
    unsigned int ebo = 0;
    unsigned int texture = 0;
    unsigned int size;
};


#endif //BACHELOR_LINE_H
