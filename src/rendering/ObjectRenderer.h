//
// Created by viking on 5/6/19.
//

#ifndef BACHELOR_OBJECTRENDERER_H
#define BACHELOR_OBJECTRENDERER_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <string>
#include "../include/stb_image.h"
#include "Shader.h"
#include "Camera.h"
#include "Mesh.h"


void framebuffer_size_callback(GLFWwindow*, int, int); //resize callback function
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow*);

class ObjectRenderer {

public:
    Shader shader;

    int init(cv::Mat);
    void run();

private:
    GLFWwindow *window;
    Mesh m;
};


#endif //BACHELOR_OBJECTRENDERER_H
