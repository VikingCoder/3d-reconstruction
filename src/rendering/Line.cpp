//
// Created by viking on 7/9/19.
//

#include <iostream>
#include "Line.h"


float v[] = {
        -0737.0f, -0163.0f, 3.07f,
        110.0f, -30.0f, -31.0f
};


void Line::init() {
    //loadMesh();

    // Create Buffers
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    //glGenBuffers(1, &ebo);

    // Bind buffers
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(v), v, GL_STATIC_DRAW);

    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ind), ind, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);

    glEnableVertexAttribArray(0);

}

void Line::draw() {
    glLineWidth(7.375f);

    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //glDrawElements(GL_TRIANGLE_STRIP, 35, GL_UNSIGNED_INT, 0);
    glDrawArrays(GL_LINES, 0, 2);

}