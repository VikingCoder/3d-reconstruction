//
// Created by viking on 5/6/19.
//

#include "ObjectRenderer.h"
#include "Line.h"


const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const char *TITLE = "Object";

Camera mCamera;
Camera pnpCamera;
float mLastX = SCREEN_WIDTH / 2.0f;
float mLastY = SCREEN_HEIGHT / 2.0f;
bool mFirstMouse = true;

float mDeltaTime = 0.0f;
float mLastFrame = 0.0f;

Line f;

cv::Mat glViewMatrix;
bool isPnPCamera = true;
bool cursorIsLocked = false;
bool cameraIsPressed = false;
bool fixPointerIsPressed = false;

int ObjectRenderer::init(cv::Mat viewMat) {
    m = Mesh(true, Image::p.getWidthX(), Image::p.getHeightY(), Image::p.getDepthZ());

    glViewMatrix = viewMat;

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, TITLE, nullptr, nullptr);

    if(window == nullptr) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        glfwTerminate();
        return -1;
    }

    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    mCamera = Camera(glm::vec3(0.0f, 0.0f, 3.0f));
    pnpCamera = Camera(glm::vec3(0.0f, 0.0f, 3.0f));
    shader = Shader("src/rendering/shaders/vertex.vs.glsl", "src/rendering/shaders/frag.fs.glsl");
    shader.setInt("ourTexture", 0);
    m.init();
    f.init();
}

void ObjectRenderer::run() {

    while(!glfwWindowShouldClose(window)) {
        // Frame time logic
        float currentFrame = glfwGetTime();
        mDeltaTime = currentFrame - mLastFrame;
        mLastFrame = currentFrame;

        // input
        processInput(window);

        // clear screen
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // --------------------- Render Object -------------------------------------
        // Use shader
        shader.use();

        // pass projection matrix to shader (note that in this case it could change every frame)
        glm::mat4 projection = glm::perspective(glm::radians(mCamera.zoom), (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT, 0.1f, 100.0f);
        shader.setMat4("projection", projection);

        // camera/view transformation
        glm::mat4 view;
        if(isPnPCamera) view = pnpCamera.GetViewMatrixPnP(glViewMatrix);
        else view = mCamera.GetViewMatrix();
        shader.setMat4("view", view);

        // calculate the model matrix for each object and pass it to shader before drawing
        glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
        model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
        shader.setMat4("model", model);

        // Render
        m.draw();
        // ------------------- End Render Object -----------------------------------

        // Render Frustum

//        view = pnpCamera.GetViewMatrixPnP(glViewMatrix);
//
//        glm::vec4 c(100.0f, 100.0f, 100.0f, 100.0f);
//
//        glm::vec4 v = glm::inverse(projection * view) * c;
//
//        std::cout << v[0] << " | " <<  v[1] << " | " <<  v[2] << " | " <<  v[3] << std::endl;
//
//        f.draw();

        // End Render Frustum

        glfwSwapBuffers(window);
        // GLFW event handling
        glfwPollEvents();

    }

    glfwTerminate();

}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        mCamera.ProcessKeyboard(FORWARD, mDeltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        mCamera.ProcessKeyboard(BACKWARD, mDeltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        mCamera.ProcessKeyboard(LEFT, mDeltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        mCamera.ProcessKeyboard(RIGHT, mDeltaTime);
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS && !cameraIsPressed) {
        isPnPCamera = !isPnPCamera;
        cameraIsPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_RELEASE) {
        cameraIsPressed = false;
    }
    if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS && !fixPointerIsPressed) {
        if(cursorIsLocked) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            cursorIsLocked = false;
        }
        else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            cursorIsLocked = true;
        }
        fixPointerIsPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_X) == GLFW_RELEASE) {
        fixPointerIsPressed = false;
    }

}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (mFirstMouse)
    {
        mLastX = xpos;
        mLastY = ypos;
        mFirstMouse = false;
    }

    float xoffset = xpos - mLastX;
    float yoffset = mLastY - ypos; // reversed since y-coordinates go from bottom to top

    mLastX = xpos;
    mLastY = ypos;

    mCamera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    mCamera.ProcessMouseScroll(yoffset);
}