//
// Created by viking on 5/22/19.
//


#include <iostream>
#include "Mesh.h"
#include "../include/stb_image.h"
#include <algorithm>


//float vertices[] = {
//
//        -1.0f, -1.0f,  1.0f, 0.0f, 0.0f,
//        -1.0f,  1.0f,  1.0f, 0.0f, 0.5f,
//        -1.0f, -1.0f, -1.0f, 1.0f, 1.0f,
//        -1.0f,  1.0f, -1.0f, 0.0f, 1.0f,
//         1.0f, -1.0f,  1.0f, 0.5f, 0.0f,
//         1.0f,  1.0f,  1.0f, 0.5f, 0.5f,
//         1.0f, -1.0f, -1.0f, 1.0f, 0.0f,
//         1.0f,  1.0f, -1.0f, 1.0f, 0.5f,
//
//};
//
//float indices[] = {
//
//    1, 2, 0,
//    3, 6, 2,
//    7, 4, 6,
//    5, 0, 4,
//    6, 0, 2,
//    3, 5, 7,
//    1, 3, 2,
//    3, 7, 6,
//    7, 5, 4,
//    5, 1, 0,
//    6, 4, 0,
//    3, 1, 5,
//
//};

//float vertices[] = {
//        // positions          // colors           // texture coords
//         0.5f,  0.5f, 0.0f, 1.0f, 1.0f, // top right
//         0.5f, -0.5f, 0.0f, 1.0f, 0.0f, // bottom right
//        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, // bottom left
//        -0.5f,  0.5f, 0.0f, 0.0f, 1.0f  // top left
//};
//unsigned int indices[] = {
//        0, 1, 3, // first triangle
//        1, 2, 3  // second triangle
//};

// TODO: Build this dynamically
float vertices[] = {

        // Left face
        0.0f, 0.0f, 0.0f, 0.0f, 0.0f,       // 0
        0.0f, 0.0f, 1.0f, 0.5f, 0.0f,       // 1
        0.0f, 1.0f, 0.0f, 0.0f, 0.5f,       // 3

        0.0f, 1.0f, 1.0f, 0.5f, 0.5f,       // 4
        0.0f, 0.0f, 1.0f, 0.5f, 0.0f,       // 1
        0.0f, 1.0f, 0.0f, 0.0f, 0.5f,       // 3

        // Right Face
        0.0f, 0.0f, 1.0f, 0.5f, 0.0f,       // 1
        1.0f, 0.0f, 1.0f, 1.0f, 0.0f,       // 2
        0.0f, 1.0f, 1.0f, 0.5f, 0.5f,       // 4

        1.0f, 1.0f, 1.0f, 1.0f, 0.5f,       // 5
        1.0f, 0.0f, 1.0f, 1.0f, 0.0f,       // 2
        0.0f, 1.0f, 1.0f, 0.5f, 0.5f,       // 4

        // Top Face
        0.0f, 1.0f, 0.0f, 0.5f, 1.0f,       // 3
        0.0f, 1.0f, 1.0f, 0.5f, 0.5f,       // 4
        1.0f, 1.0f, 0.0f, 1.0f, 1.0f,       // 6

        1.0f, 1.0f, 1.0f, 1.0f, 0.5f,       // 5
        1.0f, 1.0f, 0.0f, 1.0f, 1.0f,       // 6
        0.0f, 1.0f, 0.0f, 0.5f, 1.0f,       // 3

};

float verts[35];
//int ind[12];

int ind[] = {
        0, 1, 3,
        1, 4, 3,
        1, 2, 4,
        2, 5, 4,
        3, 5, 4,
        3, 6, 5,
        2, 5, 6,
};

float vertices1[20];
float vertices2[20];

unsigned int indices1[6];
unsigned int indices2[6];

Mesh::Mesh(bool dynamicVerts, float x, float y, float z) {
    //std::reverse(indices, indices + (int)sizeof(indices)/sizeof(indices[0]));
    if(dynamicVerts) {
        buildVertices(x, y, z);
    }
}

void Mesh::init() {
    //loadMesh();

    // Create Buffers
    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    //glGenBuffers(1, &ebo);

    // Bind buffers
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ind), ind, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    applyTexture();

}

void Mesh::draw() {
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    //glDrawElements(GL_TRIANGLE_STRIP, 35, GL_UNSIGNED_INT, 0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 18);
}

void Mesh::buildVertices(float x, float y, float z) {
    float v[90] = {
            // Left face
            0.0f, 0.0f, 0.0f, 0.0f, 0.0f,       // 0
            0.0f, 0.0f, z, 0.5f, 0.0f,       // 1
            0.0f, y, 0.0f, 0.0f, 0.5f,       // 3

            0.0f, y, z, 0.5f, 0.5f,       // 4
            0.0f, 0.0f, z, 0.5f, 0.0f,       // 1
            0.0f, y, 0.0f, 0.0f, 0.5f,       // 3

            // Right Face
            0.0f, 0.0f, z, 0.5f, 0.0f,       // 1
            x, 0.0f, z, 1.0f, 0.0f,       // 2
            0.0f, y, z, 0.5f, 0.5f,       // 4

            x, y, z, 1.0f, 0.5f,       // 5
            x, 0.0f, z, 1.0f, 0.0f,       // 2
            0.0f, y, z, 0.5f, 0.5f,       // 4

            // Top Face
            0.0f, y, 0.0f, 0.5f, 1.0f,       // 3
            0.0f, y, z, 0.5f, 0.5f,       // 4
            x, y, 0.0f, 1.0f, 1.0f,       // 6

            x, y, z, 1.0f, 0.5f,       // 5
            x, y, 0.0f, 1.0f, 1.0f,       // 6
            0.0f, y, 0.0f, 0.5f, 1.0f,       // 3
    };

    for(int i = 0; i < 90; i++) {
        vertices[i] = v[i];
    }
}

void Mesh::loadMesh() {
    std::vector<cv::Point3d> modelPoints = Image::p.getModelPoints();
    std::vector<int> faces = Image::p.getFaces();
    int verticesPerFace = Image::p.getVerticesPerFace();

    int numFaces = faces.size() / verticesPerFace;

    std::cout << numFaces << std::endl;

    //verts = (float *)malloc(modelPoints.size() * 5 * sizeof(float));
    //ind = (int *)malloc(faces.size() * sizeof(int));

    for(int i = 0; i < modelPoints.size(); i++) {
        // Points
        verts[i * 5] = (float)modelPoints[i].x;
        verts[i * 5 + 1] = (float)modelPoints[i].y;
        verts[i * 5 + 2] = (float)modelPoints[i].z;
        // Texture coordinates
        switch(i) {
            case 0:
                verts[i * 5 + 3] = 0.0f;
                verts[i * 5 + 4] = 0.0f;
                break;
            case 1:
                verts[i * 5 + 3] = 0.5f;
                verts[i * 5 + 4] = 0.0f;
                break;
            case 2:
                verts[i * 5 + 3] = 1.0f;
                verts[i * 5 + 4] = 0.0f;
                break;
            case 3:
                verts[i * 5 + 3] = 0.0f;
                verts[i * 5 + 4] = 0.5f;
                break;
            case 4:
                verts[i * 5 + 3] = 0.5f;
                verts[i * 5 + 4] = 0.5f;
                break;
            case 5:
                verts[i * 5 + 3] = 1.0f;
                verts[i * 5 + 4] = 0.5f;
                break;
            case 6:
                verts[i * 5 + 3] = 0.0f;
                verts[i * 5 + 4] = 1.0f;
                break;
            default:
                break;
        }


    }


    //for(int i = 0; i < faces.size(); i++) {
    //    ind[i] = faces[i];
    //}

}

void Mesh::applyTexture() {

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    stbi_set_flip_vertically_on_load(true);

    int width, height, nrChannels;
    unsigned char *data = stbi_load("combinedTexture.jpg", &width, &height, &nrChannels, 0);
    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else {
        std::cerr << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);

    glEnable(GL_DEPTH_TEST);

}