//
// Created by viking on 5/22/19.
//

#ifndef BACHELOR_MESH_H
#define BACHELOR_MESH_H

#include <glad/glad.h>
#include <vector>
#include <string>

#include "../Image.h"


class Mesh {
public:
    Mesh() = default;
    Mesh(bool, float, float, float);

    void init();
    void draw();

private:
    unsigned int vao = 0;
    unsigned int vbo = 0;
    unsigned int ebo = 0;
    unsigned int texture = 0;
    unsigned int size;

    void buildVertices(float, float, float);
    void loadMesh();
    void applyTexture();

};


#endif //BACHELOR_MESH_H
