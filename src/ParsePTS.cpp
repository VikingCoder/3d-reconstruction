//
// Created by viking on 4/24/19.
//

#include <fstream>
#include <iostream>
#include "ParsePTS.h"


bool shouldRemove(char c) {
    switch(c) {
        case 'v':
        case 'f':
        case 's':
        case 'l':
        case '[':
        case ']':
        case ' ':
        case '>':
        case 'c':
            return true;
        default:
            return false;

    }
}

std::vector<std::string> split(std::string s, const std::string &delimiter) {
    std::vector<std::string> result;

    size_t pos = 0;
    std::string token;
    while((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        result.emplace_back(token);
        s.erase(0, pos + delimiter.length());
    }

    result.emplace_back(s);

    return result;
}

void ParsePTS::parse(const std::string &fileName) {
    //Open file
    std::ifstream file(fileName);

    std::string line;
    while(std::getline(file, line)) {

        // Get first character to decide what the line describes
        char cmd = line[0];

        switch(cmd) {
            case 'v':
                addVertex(line);
                break;
            case 's':
                addDestinationPoints(line);
                break;
            case 'f':
                addFace(line);
                break;
            case 'l':
                addFocalLength(line);
                break;
            case 'c':
                setCoords(line);
                break;
            default:
                continue;
        }



    }
}

void ParsePTS::addVertex(std::string line) {
    int coord[3];
    std::vector<std::string> points;
    std::vector<std::string> model;
    std::vector<std::string> image;

    // Remove characters that aren't needed
    line.erase(std::remove_if(line.begin(), line.end(), &shouldRemove), line.end());

    points = split(line, "-");

    if(points.size() != 2){
        std::cerr << "ERROR: Invalid file content" << std::endl;

        return;
    }

    // Get Model and Image points
    model = split(points[0], ",");
    image = split(points[1], ",");

    if(model.size() != 3){
        std::cerr << "ERROR: Invalid file content" << std::endl;
        return;
    }
    if(image.size() != 2){
        std::cerr << "ERROR: Invalid file content" << std::endl;
        return;
    }

    for(int i = 0; i < model.size(); i++) {
        coord[i] = std::stoi(model[i]);
    }
    modelPoints.emplace_back(cv::Point3d(coord[0], coord[1], coord[2]));

    for(int i = 0; i < image.size(); i++) {
        coord[i] = std::stoi(image[i]);
    }
    imagePoints.emplace_back(cv::Point2d(coord[0], coord[1]));
}

void ParsePTS::addDestinationPoints(std::string line) {
    std::vector<std::string> size;

    line.erase(std::remove_if(line.begin(), line.end(), &shouldRemove), line.end());
    size = split(line, ",");

    if(size.size() != 2){
        std::cerr << "ERROR: Invalid file content" << std::endl;

        return;
    }

    sizeX = std::stof(size[0]);
    sizeY = std::stof(size[1]);

    destinationPoints.emplace_back(cv::Point2f(0.0, 0.0));
    destinationPoints.emplace_back(cv::Point2f(sizeX, 0.0));
    destinationPoints.emplace_back(cv::Point2f(0.0, sizeY));
    destinationPoints.emplace_back(cv::Point2f(sizeX, sizeY));

}

void ParsePTS::addFace(std::string line) {
    std::vector<cv::Point2d> vertices;
    std::vector<std::string> indicesString;

    line.erase(std::remove_if(line.begin(), line.end(), &shouldRemove), line.end());
    indicesString = split(line, ",");

    if(indicesString.size() != 4) {
        std::cerr << "ERROR: Invalid file content" << std::endl;
        return;
    }

    for(int i = 0; i < 4; i++) {
        int index = std::stoi(indicesString[i]);
        index--;
        sourcePoints.emplace_back(imagePoints[index]);
        faces.emplace_back(index);
    }
}

void ParsePTS::addFocalLength(std::string line) {
    line.erase(std::remove_if(line.begin(), line.end(), &shouldRemove), line.end());
    focalLength = std::stof(line);
}

void ParsePTS::setCoords(std::string line) {
    std::vector<std::string> coordsString;

    line.erase(std::remove_if(line.begin(), line.end(), &shouldRemove), line.end());
    coordsString = split(line, ",");

    if(coordsString.size() != 3) {
        std::cerr << "ERROR: Invalid file content" << std::endl;
        return;
    }

    widthX = std::stof(coordsString[0]);
    heightY = std::stof(coordsString[1]);
    depthZ = std::stof(coordsString[2]);
}

cv::Size ParsePTS::getSize() {
    return cv::Size(sizeX, sizeY);
}

std::vector<cv::Point2d> ParsePTS::getImagePoints() {
    return imagePoints;
}

std::vector<cv::Point3d> ParsePTS::getModelPoints() {
    return modelPoints;
}

std::vector<int> ParsePTS::getFaces() {
    return faces;
}

int ParsePTS::getVerticesPerFace() {
    return verticesPerFace;
}

std::vector<cv::Point2d> ParsePTS::getSourcePoints() {
    return sourcePoints;
}

std::vector<cv::Point2d> ParsePTS::getDestinationPoints() {
    return destinationPoints;
}

double ParsePTS::getFocalLength() {
    return focalLength;
}

float ParsePTS::getWidthX() {
    return widthX;
}

float ParsePTS::getHeightY() {
    return heightY;
}

float ParsePTS::getDepthZ() {
    return depthZ;
}
